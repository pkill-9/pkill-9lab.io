(use-modules (ice-9 regex)) ;; for string-match

(define (safe-list-head lst k)
  "Returns the list if it is shorter than the cutoff."
  (if (>= (length lst) k)
      (list-head lst k)
      lst))

(define (construct-navbar pairs separator)
  ;; takes pairs of title and url, e.g.
  ;;  '(("ALL" "/archive")
  ;;   ("TAGS" "/tags"))
  ;; Takes a separator to put between the navbar items
  (let ((navbar
         (apply append
                (map (lambda (pair)
                       (let* ((title (list-ref pair 0))
                              (url (list-ref pair 1)))
                         (list (anchor title url)
                               separator)))
                     pairs))))
    (list-head navbar (- (length navbar) 1))))

(define (stylesheet name)
`(link (@ (rel "stylesheet")
        (href ,(string-append "/css/" name ".css")))))

(define (anchor content uri)
`(a (@ (href ,uri)) ,content))

(define (jumbotron content)
`(div (@ (class "jumbotron"))
    (div (@ (class "row"))
         (div (@ (class "column-info")) ,content))))

(define %cc-by-sa-link
'(a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/"))
  "Creative Commons Attribution Share-Alike 4.0 International"))

(define-public cid-theme
  (theme #:name "Haunt"
         #:layout
         (lambda (site title body)
           `((doctype "html")
             (head
              (meta (@ (charset "utf-8")))
              (meta (@ (name "viewport") (content "width=400")))
              (title ,(string-append title " — " (site-title site)))
              ,(stylesheet "reset")
              ,(stylesheet "cid"))
             (body
              (div (@ (class "container"))

              ,body)
              (div (@ (class "container"))
              (footer (@ (class "blog-footer"))
                      (p (nav
                       ,(construct-navbar
                         '(("HOME" "/")
                           ("ABOUT" "/about")
                           ("PROJECTS" "/projects")
                           ("ALL POSTS" "/archive")
                           ("TAGS" "/tags"))
                        " ● ")))))
              )))
         #:post-template
         (lambda (post)
           `((div (@ (class "post"))
             (header (h1 ,(post-ref post 'title))
                     (p (@ (class "date"))
                        ,(date->string (post-date post) "~A, ~d ~B ~Y")))
             (article ,(post-sxml post)))))
         #:collection-template
         (lambda (site title posts prefix)
           (define (post-uri post)
             (let* ((post-slug (site-post-slug site post)))
               (if (string-match "/index$" post-slug)
                   (string-append "/" (or prefix "")
                                  (substring post-slug
                                             0 (- (string-length post-slug) 6)))
                   (string-append post-slug ".html"))))
           (define (post-excerpt post)
             (let* ((post-excerpt (post-ref post 'summary)))
               (if post-excerpt
                   `((div (@ (class "excerpt"))
                          (p ,post-excerpt)))
                   "")))

           `((header (@ (class "blog-header"))
                     (h1 "MIHA.INFO")
                     "Welcome.")
             (div (@ (class "post-list"))
                   ,@(safe-list-head (map (lambda (post)
                            `(article (@ (class "post-overview"))
                                      (h2 (a (@ (href ,(post-uri post)))
                                             ,(post-ref post 'title)))
                                      (time ,(date->string (post-date post) "~b ~d, ~Y"))
                                      ,(post-excerpt post)))
                                          (posts/reverse-chronological posts)) 5))
             ))))
