<?xml version="1.0" encoding="utf-8"?><feed xmlns="http://www.w3.org/2005/Atom"><title>MIHA.INFO</title><subtitle>Recent Posts</subtitle><updated>2020-04-08T16:48:09+0100</updated><link href="miha.info/feed.xml" rel="self" /><link href="miha.info" /><entry><title>How this site was made</title><author><name>Pkill9</name><email>pkill9@runbox.com</email></author><updated>2019-07-21T12:56:00+0100</updated><link href="/how-this-site-was-made/index.html" rel="alternate" /><summary type="html">&lt;p&gt;This site was made using &lt;a href=&quot;https://dthompson.us/projects/haunt.html&quot;&gt;Haunt&lt;/a&gt;, a static website generator written in &lt;a href=&quot;https://www.gnu.org/software/guile/guile.html&quot;&gt;Guile-Scheme&lt;/a&gt;. It is hosted on GitLab pages (&lt;a href=&quot;https://gitlab.com/pkill-9/pkill-9.gitlab.io&quot;&gt;link to repository&lt;/a&gt;).&lt;/p&gt;&lt;p&gt;I was originally using Pelican, a static site generator written in Python, but since switching my operating system to &lt;a href=&quot;https://guix.gnu.org&quot;&gt;Guix System&lt;/a&gt; and consequently learning Guile to a reasonable degree, I switched to Haunt.&lt;/p&gt;&lt;p&gt;I ported the Pelican theme I was using (&lt;a href=&quot;https://github.com/hdra/Pelican-Cid&quot;&gt;Cid&lt;/a&gt;) to Haunt - tweaked the HTMl generation of the default Haunt theme to match the Cid theme, and just copied over the CSS file.&lt;/p&gt;&lt;p&gt;The workflow is very simple, you can run &lt;code&gt;haunt build&lt;/code&gt; in the root of the website configuration directory to generate the website, and &lt;code&gt;haunt serve --watch&lt;/code&gt; to view the website at &lt;code&gt;localhost:8080&lt;/code&gt; and regenerate it when any files change. Any changes are committed to the Git repository - the changes to the configuration files and the generated output are kept together.&lt;/p&gt;&lt;p&gt;I host my website on GitLab pages, so when I push changes to the Git repository, it automatically updates the website - It doesn't generate the website output (i.e. run &lt;code&gt;haunt build&lt;/code&gt; serverside), it uses the output I generated locally with &lt;code&gt;haunt build&lt;/code&gt;.&lt;/p&gt;&lt;h2&gt;Configuration&lt;/h2&gt;&lt;p&gt;There is a single configuration file &amp;quot;haunt.scm&amp;quot; in the root of the directory. This is the only file that Haunt itself uses, all other configuration files and data is customised within this file. In my site, I use these custom configuration files and data:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;&lt;code&gt;builders.scm&lt;/code&gt; - Guile module with procedures for building non-standard pages like the archive page&lt;/li&gt;&lt;li&gt;&lt;code&gt;themes&lt;/code&gt; - directory containing Guile modules with procedures for building the theme that is passed to the &amp;quot;site&amp;quot; config&lt;/li&gt;&lt;li&gt;&lt;code&gt;posts&lt;/code&gt; - Directory containing the posts for the website, written in Markdown.&lt;/li&gt;&lt;li&gt;&lt;code&gt;pages&lt;/code&gt; - Directory containing pages for the website, which appear in the top navigation bar, for example the &amp;quot;Projects&amp;quot; page.&lt;/li&gt;&lt;li&gt;&lt;code&gt;drafts&lt;/code&gt; - Directory containing unpublished drafts. These aren't included in the generated website.&lt;/li&gt;&lt;li&gt;&lt;code&gt;static-assets&lt;/code&gt; - Directory containing assets for the website, such as fonts and CSS files.&lt;/li&gt;&lt;li&gt;&lt;code&gt;output&lt;/code&gt; - Directory containing the generated output of the website.&lt;/li&gt;&lt;/ul&gt;&lt;hr /&gt;&lt;p&gt;Fun fact: I just accidentally erased this post by moving it into the &amp;quot;output&amp;quot; directory (I meant to put it in the &amp;quot;posts&amp;quot; directory) and running &lt;code&gt;haunt build&lt;/code&gt;. I recovered it by running as root &lt;code&gt;grep -a -C 500 '2019-07-21 11:59' /dev/mapper/home-partition | tee /tmp/recover&lt;/code&gt;, and extracting the contents from /tmp/recover (solution found from a &lt;a href=&quot;https://unix.stackexchange.com/a/98700&quot;&gt;Stackoverflow answer&lt;/a&gt;). Phew!!!&lt;/p&gt;</summary></entry><entry><title>My Guix System Desktop Setup</title><author><name>Pkill9</name><email>pkill9@runbox.com</email></author><updated>2019-07-19T16:13:00+0100</updated><link href="/my-guix-system-desktop-configuration/index.html" rel="alternate" /><summary type="html">&lt;p&gt;My Guix System configuration is stored in a &lt;a href=&quot;https://gitlab.com/pkill-9/guix-config&quot;&gt;public git repository&lt;/a&gt;.&lt;/p&gt;&lt;h1&gt;My ~/.config/guix directory layout&lt;/h1&gt;&lt;p&gt;In the root of the Guix config directory (~/.config/guix), I have three main parts of the configuration: the Guix System configuration itself (The file you run &lt;code&gt;guix system reconfigure&lt;/code&gt; with), the channels specification (Refer to the &lt;a href=&quot;https://www.gnu.org/software/guix/manual/en/guix.html#Channels&quot;&gt;manual&lt;/a&gt; for an explanation), and local mirrors of my personal repositories (Publically available at &lt;a href=&quot;https://gitlab.com/pkill-9&quot;&gt;my Gitlab profile&lt;/a&gt;).&lt;/p&gt;&lt;h2&gt;Reconfiguring with local repositories&lt;/h2&gt;&lt;p&gt;By adding the local mirrors of my personal repositories to the Guile load path, I can make changes to those repositories and instantly build a system that incorporates those changes, without requiring an environment variable specified. Since these additions are specified in the file, I can reconfigure with root and the same load path additions will be applied, so I don't need to pass an environment variable.&lt;/p&gt;&lt;p&gt;I do this by adding a subdirectory of the config file's directory (which is in ~/.config/guix) to Guile's load path:&lt;/p&gt;&lt;pre&gt;&lt;code&gt;(use-modules (guix gexp)) ;; for local-file*

(define this-file
  (local-file-absolute-file-name
   (local-file
    (assoc-ref (current-source-location) 'filename))))

(define this-directory
  (dirname this-file))

(define (warncheck-path path)
  (if (file-exists? path)
      #t
      (format (current-error-port) (string-append &amp;quot;WARNING: couldn't find added load-path &amp;quot; path &amp;quot;\n&amp;quot;))))

(define (relative-add-to-load-path path-relative-to-this-file)
  (let* ((absolute-path (string-append
                         this-directory
                         path-relative-to-this-file)))
    (add-to-load-path absolute-path)
    (warncheck-path absolute-path)))

;; Adding these local paths to the load paths overrides the guix pull'd modules,
;;   and allows me to modify the channels and instantly test building a system
;;   with the changes. Since they are added using the file, root also gets
;;   these load paths.
(relative-add-to-load-path
   &amp;quot;/local-channels/pkill9-free&amp;quot;)
(relative-add-to-load-path
 &amp;quot;/local-channels/pkill9-nonfree&amp;quot;)&lt;/code&gt;&lt;/pre&gt;&lt;h2&gt;Splitting up the configuration into multiple files&lt;/h2&gt;&lt;p&gt;I do this by adding the directory of the config file to Guile's load path, which will import the other configuration files which have operating system configurations assigned to a variable, and then inheriting that variable in the operating system configuration.&lt;/p&gt;&lt;p&gt;This part adds the config file's directory to Guile's load path, using the &amp;quot;relative-add-to-load-path&amp;quot; function (seen in the previous section):&lt;/p&gt;&lt;pre&gt;&lt;code&gt;(relative-add-to-load-path &amp;quot;&amp;quot;)&lt;/code&gt;&lt;/pre&gt;&lt;p&gt;I use this to import variables from other system configurations from the same directory - This allows me to reconfigure the system to a lightweight installation first, then boot into that, and then iteratively reconfigure with more complexity (e.g. more stuff needed to compile).&lt;/p&gt;&lt;p&gt;I create a configuration that inherits a system configuration from one of the other config files:&lt;/p&gt;&lt;pre&gt;&lt;code&gt;(define my-system-configuration
  (operating-system (inherit system-configuration-from-other-file)
    ...))&lt;/code&gt;&lt;/pre&gt;&lt;h2&gt;Guix channels: Local or remote repositories&lt;/h2&gt;&lt;p&gt;Guix channels provide a way to add package definitions to your Guix revision that are provided from other sources than the official Guix repository. See the &lt;a href=&quot;https://guix.gnu.org/manual/en/guix.html#Channels&quot;&gt;channels section&lt;/a&gt; of the Guix manual for more information.&lt;/p&gt;&lt;p&gt;My channels.scm uses local mirrors of my personal repositories. This means I can modify the repositories and pull the changes when i run &lt;code&gt;guix pull&lt;/code&gt;. Currently however, the changes need to be committed to the repository as &lt;code&gt;guix pull&lt;/code&gt; assumes the URLs are git repositories (Adding the &amp;quot;file://&amp;quot; prefix to the path means that git will use a local path).&lt;/p&gt;&lt;p&gt;I define a new function called &amp;quot;local-or-remote-channel&amp;quot; that uses a local URI for a channel if it's available (This is used in place of &lt;code&gt;(channel ...)&lt;/code&gt;):&lt;/p&gt;&lt;pre&gt;&lt;code&gt;(define (local-or-remote-url local-channel-name remote-channel-url)
  (let* ((local-channel-path
          (lambda (channel-name)
            (string-append (getenv &amp;quot;HOME&amp;quot;)
                           &amp;quot;/.config/guix/local-channels/&amp;quot;
                           channel-name)))
         (local-channel-git-path
          (lambda (channel-name)
            (string-append &amp;quot;file://&amp;quot;
                           (local-channel-path
                            channel-name)))))
    (if (file-exists? (local-channel-path local-channel-name))
        (local-channel-git-path local-channel-name)
        remote-channel-url)))

(define (local-or-remote-channel channel-name remote-channel-url)
  (channel
   (name (string-&amp;gt;symbol channel-name))
   (url
    (local-or-remote-url channel-name remote-channel-url))))&lt;/code&gt;&lt;/pre&gt;&lt;p&gt;I also use a new function called &amp;quot;extra-channels&amp;quot; which simply takes channels as arguments and automatically appends it to %default-channels:&lt;/p&gt;&lt;pre&gt;&lt;code&gt;(define extra-channels
  (lambda extra-channels
    (append extra-channels
            %default-channels)))&lt;/code&gt;&lt;/pre&gt;&lt;p&gt;I then use &lt;code&gt;(local-or-remote-channel ...)&lt;/code&gt; in &lt;code&gt;(extra-channels ...)&lt;/code&gt;, which is what &lt;code&gt;guix pull&lt;/code&gt; will evaluate channels.scm to.:&lt;/p&gt;&lt;pre&gt;&lt;code&gt;(extra-channels (local-or-remote-channel
                 &amp;quot;pkill9-free&amp;quot;
                 &amp;quot;https://gitlab.com/pkill-9/guix-packages-free.git&amp;quot;)
                (local-or-remote-channel
                 &amp;quot;pkill9-nonfree&amp;quot;
                 &amp;quot;https://gitlab.com/pkill-9/guix-packages-nonfree.git&amp;quot;))&lt;/code&gt;&lt;/pre&gt;&lt;h1&gt;Using an inferior for a custom version of the linux kernel&lt;/h1&gt;&lt;p&gt;Since Guix produces a new store path if any of a package's inputs change, the package will be compiled again to produce this store path. Since the kernel takes a few hours to compile, I make it an inferior so it's inputs are frozen and thus I can use a newer Guix revision without recompiling the kernel. For more information, see the page in the Guix manual on &lt;a href=&quot;https://guix.gnu.org/manual/en/guix.html#Inferiors&quot;&gt;inferiors&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;I created a function in my pkill9-nonfree repository's linux-nonfree.scm module that takes a guix commit, a URL pointing to my pkill9-nonfree repository, and a commit for my pkill9-nonfree repository:&lt;/p&gt;&lt;pre&gt;&lt;code&gt;(define-public (get-linux-nonfree-inferior guix-commit
                                           channel-url
                                           channel-commit)
  (first
   (lookup-inferior-packages
    (inferior-for-channels
     (list (channel
            ;; Guix commit previously used to build linux-nonfree
            (name 'guix)
            (url &amp;quot;https://git.savannah.gnu.org/git/guix.git&amp;quot;)
            (commit guix-commit))
           (channel
            ;; Linux-nonfree
            (name 'pkill9-nonfree)
            (url
             channel-url)
            (commit
             channel-commit))))
    (package-name linux))))&lt;/code&gt;&lt;/pre&gt;&lt;p&gt;I use this function in the &lt;code&gt;kernel&lt;/code&gt; field of my system configuration:&lt;/p&gt;&lt;pre&gt;&lt;code&gt;(define proprietary-desktop-os
  (operating-system (inherit desktop-os)
   ;;(kernel linux) ;; Non-inferior linux-nonfree kernel package.
   (kernel (get-linux-nonfree-inferior
            &amp;quot;48f19e60c4677e392ee2c23f28098cfcaf9d1710&amp;quot;
            (string-append &amp;quot;file:///home/itsme/.config/guix&amp;quot;
                           &amp;quot;/local-channels/pkill9-guix-packages-nonfree&amp;quot;)
            &amp;quot;b15512d5dcf19c8aa905d0afb6f1ff631eaa2c06&amp;quot;))
   ...)&lt;/code&gt;&lt;/pre&gt;&lt;h1&gt;The FHS service&lt;/h1&gt;&lt;p&gt;I built an FHS service that allows me to run downloaded binaries built for FHS-respecting Linux distributions. See &lt;a href=&quot;../guix-fhs-service&quot;&gt;my other post&lt;/a&gt; for more information.&lt;/p&gt;</summary></entry><entry><title>How to write a post</title><author><name>Pkill9</name><email>pkill9@runbox.com</email></author><updated>2019-06-25T22:14:00+0100</updated><link href="/how-to-write-a-post/index.html" rel="alternate" /><summary type="html">&lt;p&gt;Anything worth doing is worth doing poorly. So just write something.&lt;/p&gt;</summary></entry><entry><title>How the Guix FHS binary compatibility service works</title><author><name>Pkill9</name><email>pkill9@runbox.com</email></author><updated>2019-06-24T22:30:00+0100</updated><link href="/guix-fhs-service/index.html" rel="alternate" /><summary type="html">&lt;p&gt;This post attempts to explain how my Guix FHS compatibility service works. (&lt;a href=&quot;https://gitlab.com/pkill-9/guix-packages-free/blob/master/pkill9/services/fhs.scm&quot;&gt;Link to service&lt;/a&gt;/&lt;a href=&quot;https://gitlab.com/pkill-9/guix-packages-free&quot;&gt;Link to channel&lt;/a&gt;/&lt;a href=&quot;https://gitlab.com/pkill-9/guix-config/-/blob/cd0f374fd13a672effd2efead1c934d9a7afed41/desktop-config.scm#L173&quot;&gt;Link to example system configuration with the FHS service added&lt;/a&gt;)&lt;/p&gt;&lt;hr /&gt;&lt;p&gt;The FHS (&lt;a href=&quot;https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard&quot;&gt;Filesystem Hierarchy Standard&lt;/a&gt;) binary compatibility service is a service I wrote for Guix that adds support for running portable binaries typically downloaded from a website, including AppImages.&lt;/p&gt;&lt;h2&gt;How portable binaries are compiled&lt;/h2&gt;&lt;p&gt;Binaries are compiled against a system that assumes all the libs are in FHS-specific directories in the root directory, such as /share, /bin, /lib.&lt;/p&gt;&lt;p&gt;The binary is compiled with a specified path to the glibc interpreter, which is assumed to be in [the prefix of glibc, which is /, so the interpreter is assumed to be in] an FHS-specific directory - /lib or /lib64. The interpreter then looks for the shared libraries that the binary needs - It's not the binary that looks for the shared libraries, it's glibc that looks for them.&lt;/p&gt;&lt;h2&gt;glibc&lt;/h2&gt;&lt;p&gt;By default, glibc looks for these libraries in &amp;quot;&amp;lt;glibc's compilation prefix&amp;gt;/lib&amp;quot; - On an FHS distribution, the prefix is &amp;quot;/&amp;quot;, so it looks for libraries in /lib. On Guix however, the prefix is /gnu/store/...-glibc-&amp;lt;version&amp;gt;, so it will by default look for libraries in /gnu/store/...-glibc-&amp;lt;version&amp;gt;/lib - you can see this by running a binary with &lt;code&gt;strace -o &amp;lt;log-output&amp;gt;&lt;/code&gt;, it will be looking for shared libraries in this directory.&lt;/p&gt;&lt;p&gt;Since we can't compile glibc on guix with the prefix as &amp;quot;/&amp;quot; (I assume not, it probably is possible, but maybe it causes other problems, also is probably harder to maintain), and we don't want to put all the shared libraries for the binary into glibc's store path (That would require recomiling glibc each time we want to add or remove libraries), we use a handy feature provided by glibc upstream that tells glibc additional paths to look for shared libraries in: ldconfig.&lt;/p&gt;&lt;p&gt;However, Glibc in guix is built without ldconfig support (a snippet in the package definition for glibc patches a configuration file in the source, that configures glibc to build without ldconfig support). This is because on foreign distributions, there may be an ldconfig cache file that tells glibc which directories to look for additional shared libraries in, by default at /etc/ld.so.cache.&lt;/p&gt;&lt;p&gt;So we need to provide the glibc interpreter where fhs-built binaries expect to find it, and it needs to be built with ldconfig support and we need to provide it with an ldconfig cache file that tells it where to find our guix-packaged shared libraries.&lt;/p&gt;&lt;h2&gt;The solution&lt;/h2&gt;&lt;p&gt;Build glibc with the snippet removed that disables ldconfig, place a link from /lib64/ld-linux-x86-64.so.2 to &amp;lt;glibc package&amp;gt;/lib/ld-linux-x86-64.so.2 (to provide the FHS-built binaries the glibc interpreter where they expect to find it), and place an ldconfig cache file - created using a list of all our desired libraries to provide the FHS-built binaries - at /etc/ld.so.cache (The default place glibc expects to find it).&lt;/p&gt;&lt;h2&gt;Implementing the solution&lt;/h2&gt;&lt;p&gt;Using the special-files-service.&lt;/p&gt;&lt;h2&gt;Additional profile packages and additional-special-files&lt;/h2&gt;&lt;p&gt;These fields don't function any differently to the services they extend, they just make the system configuration better organised, as it makes it clear to the administrator why these special-files and system packages have been added.&lt;/p&gt;&lt;p&gt;Electron/chromium binaries require certain fonts available, or they fail catastrphically (Why this non-graceful-failure has been overlooked is beyond me), so additional packages to add to the system profile can be provided in the fhs-support service. This field is just to keep the system configuration organised, so admins know why these packages have been added to the system profile.&lt;/p&gt;&lt;p&gt;Qt applications have a hardcoded xkb path, which if not found cause keyboard input to fail.&lt;/p&gt;</summary></entry><entry><title>How to add a modified kernel built using a previous guix commit to your system configuration</title><author><name>Pkill9</name><email>pkill9@runbox.com</email></author><updated>2018-12-09T18:00:00Z</updated><link href="/guix-inferior-kernel/index.html" rel="alternate" /><summary type="html">&lt;hr /&gt;&lt;p&gt;&lt;strong&gt;This article is no longer relevant, you can use an inferior package as a kernel in upstream Guix now&lt;/strong&gt;&lt;/p&gt;&lt;hr /&gt;&lt;p&gt;Note: Until a patch is added to Guix upstream, you will rely on a modified version of guix when building a system from your Guix configuration file, which means keeping a copy of it in a directory and running &lt;code&gt;./pre-inst-env guix system reconfigure &amp;lt;system-configuration.scm&amp;gt;&lt;/code&gt;.&lt;/p&gt;&lt;p&gt;Prerequisites:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;The system is running GuixSD. This guide is unsuitable for a system that is running the Guix package manager on a foreign distribution.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Your modified kernel is in a git repository that can be used as a Guix channel.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;You have the repository for Guix checked out and built with the following patch:&lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;pre&gt;&lt;code&gt;diff --git a/gnu/system.scm b/gnu/system.scm
index a5a8f40d6..3c3fe94ad 100644
--- a/gnu/system.scm
+++ b/gnu/system.scm
@@ -21,6 +21,7 @@
 ;;; along with GNU Guix.  If not, see &amp;lt;http://www.gnu.org/licenses/&amp;gt;.
 
 (define-module (gnu system)
+  #:use-module (guix inferior)
   #:use-module (guix store)
   #:use-module (guix monads)
   #:use-module (guix gexp)
@@ -906,8 +907,8 @@ listed in OS.  The C library expects to find it under
 (define (kernel-&amp;gt;boot-label kernel)
   &amp;quot;Return a label for the bootloader menu entry that boots KERNEL.&amp;quot;
   (string-append &amp;quot;GNU with &amp;quot;
-                 (string-titlecase (package-name kernel)) &amp;quot; &amp;quot;
-                 (package-version kernel)
+                 (string-titlecase (inferior-package-name kernel)) &amp;quot; &amp;quot;
+                 (inferior-package-version kernel)
                  &amp;quot; (beta)&amp;quot;))
 
 (define (store-file-system file-systems)&lt;/code&gt;&lt;/pre&gt;&lt;p&gt;Explanation of this patch: The functions used to get the name and version of the package passed to the 'kernel' field of the operating-system declaration in the Guix system configuration are incompatible with 'inferior' packages, so this patch simply changes these functions to the functions that specifically get the name and version of an inferior package - as a result, this modified version of Guix will be incompatible with a normal/non-inferior package. &lt;em&gt;Hopefully in the future, upstream Guix will check if the package is an inferior and handle it transparently&lt;/em&gt;&lt;/p&gt;&lt;p&gt;To build the patched version of guix,&lt;/p&gt;&lt;ol&gt;&lt;li&gt;&lt;p&gt;clone the Guix repository (&lt;code&gt;git clone https://git.savannah.gnu.org/git/guix.git&lt;/code&gt;) and &lt;code&gt;cd&lt;/code&gt; into it.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Patch this repository with the patch above (copy it into a file and run &lt;code&gt;git apply &amp;lt;patchfile&amp;gt;&lt;/code&gt;)&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Enter a build environment that satisfies the build requirements for guix (&lt;code&gt;guix environment guix&lt;/code&gt;)&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Configure and build this Guix - Note that the configure command requires --localstatedir to be set. (&lt;code&gt;./configure --localstatedir=/var &amp;amp;&amp;amp; make&lt;/code&gt;)&lt;/p&gt;&lt;/li&gt;&lt;/ol&gt;&lt;hr /&gt;&lt;p&gt;Now modify your Guix system configuration file like so:&lt;/p&gt;&lt;pre&gt;&lt;code&gt;(use-modules
  (gnu)
  ;;--for inferiors--
  (guix inferior)
  (guix channels)
  (srfi srfi-1) ;; for 'first'
)

(define channels
  ;; This is the old revision of guix from which we want to
  ;; extract linux-libre, combined with our custom package
  ;; that modifies linux-libre
  (list (channel
     ;; Old revision of guix that we have used to build
     ;; our modified linux-libre package against
         (name 'guix)
         (url &amp;quot;https://git.savannah.gnu.org/git/guix.git&amp;quot;)
         (commit
          &amp;quot;&amp;lt;commit_that_corresponds_to_previously_built_custom_kernel&amp;gt;&amp;quot;))
        (channel
     ;; Our modified linux package
         (name 'custom-modified-linux)
         (url &amp;quot;https://gitlab.com/modified-linux/modified-linux.git&amp;quot;))
     ;; I'm not sure, but you may be able to point to a local git repository
     ;; by setting the URL as &amp;quot;file:///path/to/repository&amp;quot;.
    ))

(define our-modified-linux
  (first
    (lookup-inferior-packages
      ;; 'name-of-our-modified-linux-package' is searched for like how guix
      ;; searches for the package when you run `guix package -i &amp;lt;package_name&amp;gt;`
      (inferior-for-channels channels)
      &amp;quot;name-of-our-modified-linux-package&amp;quot;)))

(operating-system
  (kernel our-modified-linux)
  ...&lt;/code&gt;&lt;/pre&gt;&lt;p&gt;Now to reconfigure your system using our patched Guix, we will run the following command in the Guix repository, with the assumption we are running as a user with sudo privilege: &lt;code&gt;sudo -E ./pre-inst-env guix system reconfigure your-guix-system-configuration.scm&lt;/code&gt;.&lt;/p&gt;&lt;p&gt;Your system should now be updated without needing to recompile your custom kernel.&lt;/p&gt;</summary></entry><entry><title>First post!</title><author><name>Pkill9</name><email>pkill9@runbox.com</email></author><updated>2018-03-13T18:00:00Z</updated><link href="/hello/index.html" rel="alternate" /><summary type="html">&lt;p&gt;Hello, world!&lt;/p&gt;</summary></entry></feed>