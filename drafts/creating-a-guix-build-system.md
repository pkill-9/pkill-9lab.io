title: Creating a Guix build system
date: 2019-07-23 09:19
---

guix/build-system has modules containing code run on the "host side" (code that's used "outside" a build environment), guix/build is for code run in the builder (code that's used "inside" a build environment).