(define (archive-page site posts)
  (define (post-uri post)
    (let* ((post-slug (site-post-slug site post)))
      (if (string-match "/index$" post-slug)
          (string-append "/"
                         (substring post-slug
                                    0 (- (string-length post-slug) 6)))
          (string-append post-slug ".html"))))
  (define body
    `((div (@ (class "post")) ;;Workaround to get title the same size as a post title.
      (header (h1 "All Posts")))

      (div (@ (class "post-list"))
           ,@(map (lambda (post)
                    `(article (@ (class "post-overview"))
                              (h2 (a (@ (href ,(post-uri post)))
                                     ,(post-ref post 'title)))
                              (time ,(date->string (post-date post) "~b ~d, ~Y"))))
                  (posts/reverse-chronological posts))
             )))

  (make-page "archive/index.html"
             (with-layout cid-theme site "Archive" body)
             sxml->html))

(define (tags-page site posts)
  (define body
    `((div (@ (class "post"))
      (header (h1 "Tags")))
      "This page is not yet constructed."))

  (make-page "tags/index.html"
             (with-layout cid-theme site "Tags" body)
             sxml->html))

(define (arbitrary-pages site posts)
  (define pages
    (read-posts "pages"
                (site-file-filter site)
                (site-readers site)
                (site-default-metadata site)))
  (map (lambda (page)
       (make-page (string-append (match:prefix (string-match "\\..+$" (basename (post-file-name page)))) "/index.html") ;; match:prefix gets filename without extension
                  (render-post cid-theme site page)
                  sxml->html))
       pages))
