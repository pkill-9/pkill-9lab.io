title: About
date: 2019-07-20 15:36
---

This website was created using the [Haunt static site generator](https://dthompson.us/projects/haunt.html). The theme is a port of the Pelican [Cid](https://github.com/hdra/Pelican-Cid) theme. It is hosted on [Gitlab pages](https://gitlab.com/pkill-9/pkill-9.gitlab.io).

For a more in-depth explanation of how this website was made, see my [post](/how-this-site-was-made).