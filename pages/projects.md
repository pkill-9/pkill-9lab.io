title: Projects
date: 2019-07-20 15:36
---

# Guix
- [Guix FHS support service](https://gitlab.com/pkill-9/guix-packages-free/blob/master/pkill9/services/fhs.scm)
- [My free Guix channel packages](https://gitlab.com/pkill-9/guix-packages-free/tree/master/pkill9/packages)
- [My non-free Guix channel packages](https://gitlab.com/pkill-9/guix-packages-nonfree/tree/master/pkill9/packages)
- [Guix libfinder](https://gitlab.com/pkill-9/guix-libfinder)
- [Guix module creator](https://gitlab.com/pkill-9/guix-module-creator)

# LÖVE
- [Twin stick shooter](https://gitlab.com/pkill-9/love/tree/master/twinstick)

# bspwm
- [A bspwm config I made](https://gitlab.com/pkill-9/bspwm-custom)

# Haunt
- [This site](https://gitlab.com/pkill-9/pkill-9.gitlab.io)
