;;; Haunt --- Static site generator for GNU Guile
;;; Copyright © 2016 David Thompson <davet@gnu.org>
;;;
;;; This file is part of Haunt.
;;;
;;; Haunt is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Haunt is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Haunt.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (haunt site)
         (haunt reader)
         (haunt reader skribe)
         (haunt reader commonmark)
         (haunt asset)
         (haunt page)
         (haunt post)
         (haunt html)
         (haunt utils)
         (haunt builder blog)
         (haunt builder atom)
         (haunt builder assets)
         (srfi srfi-19)
         (ice-9 rdelim)
         (ice-9 match)
         (web uri))

(load "themes/cid.scm")
(load "builders.scm")

(define (better-slug-maker-with-date post)
  (let* ((date (date->string (post-date post) "~d"))
         (month (date->string (post-date post) "~B"))
         (year (date->string (post-date post) "~Y"))
         (filename-without-extension (match:prefix (string-match "\\..+$" (basename (post-file-name post)))))
         (post-slug (if (string-match "^....-..-..-" filename-without-extension)
                        (match:suffix (string-match "^....-..-..-" filename-without-extension))
                        filename-without-extension)))
    (string-append year "/" month "/" date "/" post-slug "/index")))

(define (better-slug-maker post)
  (let* ((filename-without-extension
          (match:prefix
           (string-match "\\..+$"
                         (basename (post-file-name post)))))
         (post-slug (if (string-match "^....-..-..-"
                                      filename-without-extension)
                        (match:suffix (string-match "^....-..-..-"
                                                    filename-without-extension))
                        filename-without-extension)))
    (string-append post-slug "/index")))


(site #:build-directory "output"
      #:make-slug better-slug-maker
      #:title "MIHA.INFO"
      #:domain "miha.info"
      #:default-metadata
      '((author "Pkill9")
        (email  "pkill9@runbox.com"))
      #:readers (list sxml-reader skribe-reader commonmark-reader)
      #:builders (list (blog #:theme cid-theme
                             #:collections
                             `(("Home" "index.html" ,posts/reverse-chronological))
                             )
                       (atom-feed)
                       (atom-feeds-by-tag)
                       archive-page
                       tags-page
                       arbitrary-pages
                       (static-directory "static-assets" "")))
