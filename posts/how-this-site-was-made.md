title: How this site was made
date: 2019-07-21 12:56
---

This site was made using [Haunt](https://dthompson.us/projects/haunt.html), a static website generator written in [Guile-Scheme](https://www.gnu.org/software/guile/guile.html). It is hosted on GitLab pages ([link to repository](https://gitlab.com/pkill-9/pkill-9.gitlab.io)).

I was originally using Pelican, a static site generator written in Python, but since switching my operating system to [Guix System](https://guix.gnu.org) and consequently learning Guile to a reasonable degree, I switched to Haunt.

I ported the Pelican theme I was using ([Cid](https://github.com/hdra/Pelican-Cid)) to Haunt - tweaked the HTMl generation of the default Haunt theme to match the Cid theme, and just copied over the CSS file.

The workflow is very simple, you can run `haunt build` in the root of the website configuration directory to generate the website, and `haunt serve --watch` to view the website at `localhost:8080` and regenerate it when any files change. Any changes are committed to the Git repository - the changes to the configuration files and the generated output are kept together.

I host my website on GitLab pages, so when I push changes to the Git repository, it automatically updates the website - It doesn't generate the website output (i.e. run `haunt build` serverside), it uses the output I generated locally with `haunt build`.

## Configuration

There is a single configuration file "haunt.scm" in the root of the directory. This is the only file that Haunt itself uses, all other configuration files and data is customised within this file. In my site, I use these custom configuration files and data:

 - `builders.scm` - Guile module with procedures for building non-standard pages like the archive page
 - `themes` - directory containing Guile modules with procedures for building the theme that is passed to the "site" config
 - `posts` - Directory containing the posts for the website, written in Markdown.
 - `pages` - Directory containing pages for the website, which appear in the top navigation bar, for example the "Projects" page.
 - `drafts` - Directory containing unpublished drafts. These aren't included in the generated website.
 - `static-assets` - Directory containing assets for the website, such as fonts and CSS files.
 - `output` - Directory containing the generated output of the website.

---

Fun fact: I just accidentally erased this post by moving it into the "output" directory (I meant to put it in the "posts" directory) and running `haunt build`. I recovered it by running as root `grep -a -C 500 '2019-07-21 11:59' /dev/mapper/home-partition | tee /tmp/recover`, and extracting the contents from /tmp/recover (solution found from a [Stackoverflow answer](https://unix.stackexchange.com/a/98700)). Phew!!!