title: How to add a modified kernel built using a previous guix commit to your system configuration
date: 2018-12-09 18:00
modified: 2019-04-16
---

---

**This article is no longer relevant, you can use an inferior package as a kernel in upstream Guix now**

---

Note: Until a patch is added to Guix upstream, you will rely on a modified version of guix when building a system from your Guix configuration file, which means keeping a copy of it in a directory and running `./pre-inst-env guix system reconfigure <system-configuration.scm>`.

Prerequisites:

- The system is running GuixSD. This guide is unsuitable for a system that is running the Guix package manager on a foreign distribution.

- Your modified kernel is in a git repository that can be used as a Guix channel.

- You have the repository for Guix checked out and built with the following patch:
```
diff --git a/gnu/system.scm b/gnu/system.scm
index a5a8f40d6..3c3fe94ad 100644
--- a/gnu/system.scm
+++ b/gnu/system.scm
@@ -21,6 +21,7 @@
 ;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
 
 (define-module (gnu system)
+  #:use-module (guix inferior)
   #:use-module (guix store)
   #:use-module (guix monads)
   #:use-module (guix gexp)
@@ -906,8 +907,8 @@ listed in OS.  The C library expects to find it under
 (define (kernel->boot-label kernel)
   "Return a label for the bootloader menu entry that boots KERNEL."
   (string-append "GNU with "
-                 (string-titlecase (package-name kernel)) " "
-                 (package-version kernel)
+                 (string-titlecase (inferior-package-name kernel)) " "
+                 (inferior-package-version kernel)
                  " (beta)"))
 
 (define (store-file-system file-systems)
```

Explanation of this patch: The functions used to get the name and version of the package passed to the 'kernel' field of the operating-system declaration in the Guix system configuration are incompatible with 'inferior' packages, so this patch simply changes these functions to the functions that specifically get the name and version of an inferior package - as a result, this modified version of Guix will be incompatible with a normal/non-inferior package. *Hopefully in the future, upstream Guix will check if the package is an inferior and handle it transparently*

To build the patched version of guix,

1. clone the Guix repository (`git clone https://git.savannah.gnu.org/git/guix.git`) and `cd` into it.

2. Patch this repository with the patch above (copy it into a file and run `git apply <patchfile>`)

3. Enter a build environment that satisfies the build requirements for guix (`guix environment guix`)

4. Configure and build this Guix - Note that the configure command requires --localstatedir to be set. (`./configure --localstatedir=/var && make`)

---

Now modify your Guix system configuration file like so:
```
(use-modules
  (gnu)
  ;;--for inferiors--
  (guix inferior)
  (guix channels)
  (srfi srfi-1) ;; for 'first'
)

(define channels
  ;; This is the old revision of guix from which we want to
  ;; extract linux-libre, combined with our custom package
  ;; that modifies linux-libre
  (list (channel
	 ;; Old revision of guix that we have used to build
	 ;; our modified linux-libre package against
         (name 'guix)
         (url "https://git.savannah.gnu.org/git/guix.git")
         (commit
          "<commit_that_corresponds_to_previously_built_custom_kernel>"))
        (channel
	 ;; Our modified linux package
         (name 'custom-modified-linux)
         (url "https://gitlab.com/modified-linux/modified-linux.git"))
	 ;; I'm not sure, but you may be able to point to a local git repository
	 ;; by setting the URL as "file:///path/to/repository".
	))

(define our-modified-linux
  (first
    (lookup-inferior-packages
      ;; 'name-of-our-modified-linux-package' is searched for like how guix
      ;; searches for the package when you run `guix package -i <package_name>`
      (inferior-for-channels channels)
      "name-of-our-modified-linux-package")))

(operating-system
  (kernel our-modified-linux)
  ...
```

Now to reconfigure your system using our patched Guix, we will run the following command in the Guix repository, with the assumption we are running as a user with sudo privilege: `sudo -E ./pre-inst-env guix system reconfigure your-guix-system-configuration.scm`.

Your system should now be updated without needing to recompile your custom kernel.
