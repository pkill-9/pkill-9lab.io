title: How to write a post
date: 2019-06-25 22:14
---

Anything worth doing is worth doing poorly. So just write something.
